
BUILD
-----

```
docker build \
  --build-arg MQUSERNAME=user \
  --build-arg MQUSERPASSWORD=userpass \
  --build-arg MQADMINNAME=admin \
  --build-arg MQADMINPASSWORD=adminpass \
  --build-arg MQVIRTHOST=/test \
  -t mq .
```



CONFIG
------

```
docker run -it -p 15672:15672 mq cat /etc/rabbitmq/rabbitmq.config

docker run -it -p 15672:15672 mq cat /etc/rabbitmq/definitions.json
```



RUN
---

```
docker run -it -p 15672:15672 mq
```

Visit http://localhost:15672/ and login with `admin`/`adminpass`.



ERROR
------

```
[warning] <0.702.0> HTTP access denied: user 'admin' - invalid credentials
```


INSPIRATION
-----------

https://stackoverflow.com/a/42251266/2412477


