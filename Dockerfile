FROM rabbitmq:3.7-management

ARG MQADMINNAME
ARG MQADMINPASSWORD
ARG MQUSERNAME
ARG MQUSERPASSWORD
ARG MQVIRTHOST

ADD ./config/rabbitmq.config /etc/rabbitmq/
ADD ./config/definitions.json /etc/rabbitmq/
RUN chown rabbitmq:rabbitmq /etc/rabbitmq/rabbitmq.config /etc/rabbitmq/definitions.json

RUN sed -i /etc/rabbitmq/definitions.json -e "s,__rabbituser__,$MQUSERNAME,"
RUN sed -i /etc/rabbitmq/definitions.json -e "s,__rabbitadmin__,$MQADMINNAME,"
RUN sed -i /etc/rabbitmq/definitions.json -e "s,__rabbituserpassword__,$(echo -n "$MQUSERPASSWORD" | openssl dgst -sha256 -binary | openssl base64),"
RUN sed -i /etc/rabbitmq/definitions.json -e "s,__rabbitadminpassword__,$(echo -n "$MQADMINPASSWORD" | openssl dgst -sha256 -binary | openssl base64),"
RUN sed -i /etc/rabbitmq/definitions.json -e "s,__virtualhost__,$(echo "$MQVIRTHOST" | sed -e 's,/,\\\\/,g')," # Escape / with \/ within json.

